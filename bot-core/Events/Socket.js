
import chats, { manageChatWithMessage } from '../Helpers';

const configureSocketEvents = (io, bot) => {
// Emmits the current chat state on connection
  io.on('connection', (client) => {
  // it should red from the mongo and send to front
    io.emit('message', chats);

    // Message received from bot-interface
    client.on('message', ({ chatId, text }) => {
      if (chatId !== '' && text !== '') {
        bot.sendMessage(chatId, text).then((botMessage) => {
          manageChatWithMessage(chatId, botMessage);
          io.emit('message', chats);
        });
      }
    });
  });
};

export default configureSocketEvents;
