import chats, { manageChatWithMessage } from '../Helpers';

const configureBotEvents = (io, bot) => {
  bot.onText(/\/start/, (message) => {
    const chatId = message.chat.id;
    const { from } = message;

    bot.sendMessage(
      message.chat.id,
      `Hi ${from.first_name}! I am the Mr. Ziborro`,
    ).then((botMessage) => {
      manageChatWithMessage(chatId, botMessage);
      io.emit('message', chats);
    });
  });

  // Listen for any kind of message.
  bot.on('message', (message) => {
    const chatId = message.chat.id;
    manageChatWithMessage(chatId, message);
    io.emit('message', chats);
  });
};

export default configureBotEvents;
