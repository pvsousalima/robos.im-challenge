import express from 'express';
import http from 'http';
import socket from 'socket.io';
import Bot from './Bot/Bot';
import { configureSocketEvents, configureBotEvents } from './Events';
import { configureLoginAuth } from './Helpers';

const app = express();
const server = http.createServer(app);
const io = socket(server);

// received from @BotFather
const token = process.env.TOKEN;

// Create a bot that uses 'polling' to fetch new updates
const bot = new Bot(token, { polling: true });

// Bot events
configureBotEvents(io, bot);

// Socket events
configureSocketEvents(io, bot);

// Login and auth
configureLoginAuth(app);

// Server listening on port 4200
server.listen(4200);
