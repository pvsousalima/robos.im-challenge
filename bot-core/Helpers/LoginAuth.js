import Guid from 'guid';
import bodyParser from 'body-parser';
import Request from 'request';
import Querystring from 'querystring';
import cors from 'cors';

const csrfGuid = Guid.raw();
const accountKitApiVersion = 'v1.1';
const AppId = '379829512449917';
const AppSecret = 'd9bc694393d245a4572343ac40c7a8d7';
const meEndpointBaseUrl = 'https://graph.accountkit.com/v1.1/me';
const tokenExchangeBaseUrl = 'https://graph.accountkit.com/v1.1/access_token';

const configureLoginAuth = (app) => {
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(cors());

  app.get('/', (request, response) => {
    const authData = {
      appId: AppId,
      csrf: csrfGuid,
      version: accountKitApiVersion,
    };
    response.send(authData);
  });

  app.post('/login_success', (request, response) => {
    const appAccessToken = ['AA', AppId, AppSecret].join('|');
    const params = {
      grant_type: 'authorization_code',
      code: request.body.code,
      access_token: appAccessToken,
    };

    // exchange tokens
    const tokenExchangeUrl = `${tokenExchangeBaseUrl}?${Querystring.stringify(params)}`;
    Request.get({ url: tokenExchangeUrl, json: true }, (err, resp, respBody) => {
      const view = {
        user_access_token: respBody.access_token,
        expires_at: respBody.expires_at,
        user_id: respBody.id,
      };

      // get account details at /me endpoint
      const meEndpointUrl = `${meEndpointBaseUrl}?access_token=${respBody.access_token}`;
      Request.get({ url: meEndpointUrl, json: true }, (error, res, resBody) => {
      // send login_success.html
        if (resBody.phone) {
          view.phone_num = resBody.phone.number;
        } else if (resBody.email) {
          view.email_addr = resBody.email.address;
        }
        response.send(resBody);
      });
    });
  });
};

export default configureLoginAuth;
