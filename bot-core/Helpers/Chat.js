// Chats object
const chats = {};

// case messages are found push incoming msg, else create a new array of msgs
const manageChatWithMessage = (chatId, message) => {
  // chat exists?
  if (!chats[chatId]) {
    // if it does not exists lets push an empty messages array
    chats[chatId] = { messages: [] };
  }

  // fetch messages from chat
  const { messages } = chats[chatId];

  if (messages.length > 0) {
    chats[chatId].messages.push(message);
  } else {
    chats[chatId].messages = [message];
    chats[chatId].from = message.from;
  }
};

export { chats, manageChatWithMessage };
