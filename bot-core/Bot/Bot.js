import TelegramBot from 'node-telegram-bot-api';

/**
 * Represents a Bot for the challenge, returns a TelegramBot
 * from constructed from node-telegram-bot-api.
 */
class Bot {
  /**
   * Constructor for class Bot.
   * @constructor
   * @param {string} token - The token returned from @BotFather.
   * @param {object} options - The options object to construct the TelegramBot.
   */
  constructor(token, options) {
    return new TelegramBot(token, options);
  }
}

export default Bot;
