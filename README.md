# Full-stack Challenge
## ROBOS.im
### Pedro Lima

How to run
---

1. With yarn installed execute
	`yarn install` to fetch all dependencies.

2. Execute `yarn:bot` to start the bot backend.

3. Then execute a `yarn start` which will start the frontend app.

4. Go to [http://localhost:8080](Localhost) and start using the app.

5. On telegram, look for bot @ZiborroBot and have a nice chat with him ;) 

Extras
---

1. `yarn lint` will start the linter and do a check on code.
2.  `yarn test` will start Jest on testing mode.