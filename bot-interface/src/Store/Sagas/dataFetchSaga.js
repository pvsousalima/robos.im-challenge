import { call, put, takeLatest } from 'redux-saga/effects';
import axios from 'axios';

const loadState = () => axios.get('http://localhost:4200');

function* fetchData(action) {
  try {
    const { data } = yield call(loadState, action.payload);
    yield put({ type: 'DATA_FETCH_SUCCEEDED', data });
  } catch (e) {
    yield put({ type: 'DATA_FETCH_FAILED', message: e.message });
  }
}

function* dataFetchSaga() {
  yield takeLatest('DATA_FETCH_REQUESTED', fetchData);
}

export default dataFetchSaga;
