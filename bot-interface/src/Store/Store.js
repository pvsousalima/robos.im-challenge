import { Provider } from 'react-redux';
import { combineReducers, createStore, applyMiddleware } from 'redux';

import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';

import { dataSourceReducer, inputReducer, dataReducer } from '../Main/Reducers/mainReducers';
import dataFetchSaga from './Sagas/dataFetchSaga';

const sagaMiddleware = createSagaMiddleware();

/* top-level reducers */
const appReducer = combineReducers({
  dataSourceReducer,
  inputReducer,
  dataReducer,
});

/* rootReducer */
const rootReducer = (state, action) => appReducer(state, action);

/* the store */
const store = createStore(
  rootReducer,
  applyMiddleware(logger),
  applyMiddleware(sagaMiddleware),
);

// run saga
sagaMiddleware.run(dataFetchSaga);

export default Provider;
export { store, appReducer };
