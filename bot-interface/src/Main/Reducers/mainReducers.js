import { UPDATE_DATASOURCE, UPDATE_INPUT } from '../Constants';

const initialDataSourceState = {
  dataSource: {},
};

function dataSourceReducer(state = initialDataSourceState, action) {
  switch (action.type) {
    case UPDATE_DATASOURCE:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

const initialInputValue = {
  inputValue: '',
};

function inputReducer(state = initialInputValue, action) {
  switch (action.type) {
    case UPDATE_INPUT:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

const initialDataValue = {
  data: {},
};

function dataReducer(state = initialDataValue, action) {
  switch (action.type) {
    case 'DATA_FETCH_SUCCEEDED':
      return { ...state, ...action };
    default:
      return state;
  }
}

export { dataSourceReducer, inputReducer, dataReducer };
