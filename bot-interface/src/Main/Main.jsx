import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { socketConnect } from 'socket.io-react';

import ChatMenu from './Components/Chat/ChatMenu/ChatMenu';
import Messages from './Components/Chat/Messages/Messages';
import Logout from './Components/Logout/Logout';

import { CenterSection, TopHeader, Screen } from './Components/Layout/Layout';

const Main = ({
  updateDataSource, updateInput, inputValue, dataSource = {}, params, socket,
}) => {
  // Emmits the connection event
  socket.emit('connection');

  // Listens for a message containing the datasource
  socket.on('message', (incomingDataSource) => {
    updateDataSource(incomingDataSource);
  });

  // Get the current routerId from navigation
  const routerId = params.id;

  return (
    <Screen>
      {/* put elements here (logout) */}
      <TopHeader>
        <Logout />
      </TopHeader>

      <CenterSection>
        <ChatMenu
          source={dataSource}
        />
        <Messages
          source={dataSource[routerId]}
          socket={socket}
          routerId={routerId}
          updateInput={updateInput}
          inputValue={inputValue}
        />
      </CenterSection>
    </Screen>
  );
};

Main.propTypes = {
  updateDataSource: PropTypes.func.isRequired,
  updateInput: PropTypes.func.isRequired,
  inputValue: PropTypes.string.isRequired,
  dataSource: PropTypes.object.isRequired,
  params: PropTypes.object.isRequired,
  socket: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  const { dataSource } = state.dataSourceReducer;
  const { inputValue } = state.inputReducer;

  return {
    dataSource,
    inputValue,
  };
};

const mapDispatchToProps = dispatch => ({
  updateDataSource: dataSource => dispatch({
    type: 'UPDATE_DATASOURCE',
    payload: { dataSource },
  }),
  updateInput: inputValue => dispatch({
    type: 'UPDATE_INPUT',
    payload: { inputValue },
  }),
});

export default socketConnect(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main));
