import { UPDATE_DATASOURCE } from '../Constants';

export default function updateDataSource(payload) {
  return {
    type: UPDATE_DATASOURCE,
    payload,
  };
}
