import { DATA_FETCH_SUCCEEDED } from '../Constants';

export default function loadData(payload) {
  return {
    type: DATA_FETCH_SUCCEEDED,
    payload,
  };
}
