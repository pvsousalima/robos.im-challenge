import { UPDATE_INPUT } from '../Constants';

export default function updateInput(payload) {
  return {
    type: UPDATE_INPUT,
    payload,
  };
}
