import React from 'react';
import { browserHistory } from 'react-router';

const onClick = () => {
  const path = '/';
  browserHistory.push(path);
};
const Logout = () => (
  <a href="" style={{ margin: 20, textDecoration: 'none' }} onClick={onClick}>
	Logout
  </a>
);

export default Logout;
