const styles = {
  screenContainer: {
    display: 'flex',
    margin: 0,
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  topHeader: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    // position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    margin: 0,
    height: '60px',
    width: '100vw',
    backgroundColor: '#1dcaff',
    alignItems: 'center',
  },
  centerSection: {
    display: 'flex',
    width: '100vw',
    height: '100vh',
    left: 0,
    right: 0,
    padding: 0,
  },
};

export default styles;
