import React from 'react';
import renderer from 'react-test-renderer';
import {TopHeader, CenterSection, Screen} from '../Layout';

describe('Rendering Layout', () => {
	it('renders TopHeader correctly', () => {
		const tree = renderer.create(
			<TopHeader />
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders CenterSection correctly', () => {
		const tree = renderer.create(
			<CenterSection />
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders Screen correctly', () => {
		const tree = renderer.create(
			<Screen />
		).toJSON();
		expect(tree).toMatchSnapshot();
	});
})
