import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

const TopHeader = ({ children }) => (
  <div style={styles.topHeader}>
    {children}
  </div>
);

const CenterSection = ({ children }) => (
  <div style={styles.centerSection}>
    {children}
  </div>
);

const Screen = ({ children }) => (
  <div style={styles.screenContainer}>
    {children}
  </div>
);

TopHeader.propTypes = {
  children: PropTypes.array,
};

CenterSection.propTypes = {
  children: PropTypes.array,
};

Screen.propTypes = {
  children: PropTypes.array,
};

export { TopHeader, CenterSection, Screen };
