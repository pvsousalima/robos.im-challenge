import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import ChatItem from '../ChatItem/ChatItem';
import NoMessagesFoundComponent from '../NoMessagesFoundComponent/NoMessagesFoundComponent';
import styles from './styles';

const renderChats = source => Object.keys(source).map((data) => {
  const { from } = source[data];
  const { messages } = source[data];
  const lastMessage = messages.slice(-1)[0];
  const { date } = lastMessage;
  const name = from.first_name && from.last_name ?
    `${from.first_name} ${from.last_name}` :
    `${from.first_name}`;

  return (
    <Link
      key={data}
      href={`/chats/${data}`}
      to={`/chats/${data}`}
      style={styles.chatLink}
    >
      <ChatItem key={data} name={name} date={date} />
    </Link>
  );
});


const ChatMenu = ({ source }) => (
  <div style={styles.leftChatMenu}>
    {Object.keys(source).length > 0 ? renderChats(source) : <NoMessagesFoundComponent />}
  </div>
);


ChatMenu.propTypes = {
  source: PropTypes.object.isRequired,
};

export default ChatMenu;
