const styles = {
  leftChatMenu: {
    flex: 1,
    height: '100%',
    maxWidth: '20%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  chatLink: {
    textDecoration: 'none',
  },
};

export default styles;
