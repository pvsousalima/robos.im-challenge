const styles = {
  noMessagesComponent: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto',
    fontSize: 26,
  },
};

export default styles;
