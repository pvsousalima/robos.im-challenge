import React from 'react';
import styles from './styles';

const NoMessagesComponent = () => (
  <div style={styles.noMessagesComponent}>
      No chats yet!
  </div>
);

export default NoMessagesComponent;
