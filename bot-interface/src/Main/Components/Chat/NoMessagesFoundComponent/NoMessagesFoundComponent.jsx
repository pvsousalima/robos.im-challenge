import React from 'react';
import styles from './styles';

const NoMessagesFoundComponent = () => (
  <div style={styles.noMessagesFoundComponent}>
  no messages found
  </div>
);

export default NoMessagesFoundComponent;
