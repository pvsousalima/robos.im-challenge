const styles = {
  noMessagesFoundComponent: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto',
    fontSize: 16,
    margin: 20,
  },
};

export default styles;
