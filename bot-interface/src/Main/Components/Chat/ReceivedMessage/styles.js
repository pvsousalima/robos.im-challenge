const styles = {
  receivingMessage: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '80px',
    width: '160px',
    backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: 10,
    margin: 20,
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto',
    fontWeight: '200',
    fontSize: 12,
  },
};

export default styles;
