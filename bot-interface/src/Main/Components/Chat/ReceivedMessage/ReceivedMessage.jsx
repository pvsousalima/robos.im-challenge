import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './styles';

const ReceivedMessage = ({ date, text }) => (
  <div style={styles.receivingMessage}>
    <div style={{ padding: 10 }}>{text}</div>
    <div>{`${moment.unix(date).format('DD/MM/YYYY kk:mm')}`}</div>
  </div>
);

ReceivedMessage.propTypes = {
  date: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
};

export default ReceivedMessage;
