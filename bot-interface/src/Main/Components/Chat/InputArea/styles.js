const styles = {
  inputArea: {
    flex: 1,
    display: 'flex',
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
};

export default styles;
