import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

const InputArea = ({ children }) => (
  <div style={styles.inputArea}>
    {children}
  </div>
);

InputArea.propTypes = {
  children: PropTypes.shape({}).isRequired,
};

export default InputArea;
