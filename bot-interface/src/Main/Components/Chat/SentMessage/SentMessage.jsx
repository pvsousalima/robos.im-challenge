import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './styles';

const SentMessage = ({ date, text }) => (
  <div style={styles.sentMessage}>
    <div style={{ padding: 10 }}>{text}</div>
    <div>{`${moment.unix(date).format('DD/MM/YYYY kk:mm')}`}</div>
  </div>
);

SentMessage.propTypes = {
  date: PropTypes.number.isRequired,
  text: PropTypes.string.isRequired,
};

export default SentMessage;
