const styles = {
  sentMessage: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '80px',
    width: '160px',
    backgroundColor: '#1dcaff',
    borderRadius: 10,
    margin: 20,
    marginLeft: '66vw',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto',
    color: 'white',
    fontWeight: '200',
    fontSize: 12,
  },
};

export default styles;
