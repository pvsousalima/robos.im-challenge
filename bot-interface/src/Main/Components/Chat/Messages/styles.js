const styles = {
  rightMessagesContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    flex: 1,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  sendingButton: {
    outline: 'none',
    marginLeft: 20,
  },
  formControlContainer: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
  },
};

export default styles;
