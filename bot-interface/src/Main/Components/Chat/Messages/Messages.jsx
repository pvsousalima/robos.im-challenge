import React from 'react';
import PropTypes from 'prop-types';

import { Button, FormControl } from 'react-bootstrap';

import SentMessage from '../SentMessage/SentMessage';
import ReceivedMessage from '../ReceivedMessage/ReceivedMessage';
import MessagesArea from '../MessagesArea/MessagesArea';
import InputArea from '../InputArea/InputArea';
import NoMessagesComponent from '../NoMessagesComponent/NoMessagesComponent';

import styles from './styles';

const sendMessage = (socket, message) => {
  socket.emit('message', message);
};

const renderMessages = messages => messages.map((msg) => {
  const { from, date } = msg;
  if (from.is_bot) {
    return <SentMessage key={date} {...msg} />;
  }
  return <ReceivedMessage key={date} {...msg} />;
});

const renderNoMessages = () => (
  <NoMessagesComponent />
);

const renderFormWithProps = props => (
  <div style={styles.formControlContainer}>
    <FormControl
      type="text"
      value={props.inputValue}
      onChange={(e) => { props.updateInput(e.target.value); }}
    />
    <Button
      bsStyle="primary"
      style={styles.sendingButton}
      onClick={
        () => {
              sendMessage(props.socket, {
                  chatId: props.routerId,
                  text: props.inputValue,
              });
              props.updateInput('');
          }
        }
    >{'SEND'}
    </Button>
  </div>
);

const Messages = ({ ...props }) => {
  const { messages } = props.source || { messages: [] };
  return (
    <div style={styles.rightMessagesContainer}>
      <MessagesArea>
        {messages.length > 0 ? renderMessages(messages) : renderNoMessages()}
      </MessagesArea>

      <InputArea>
        {renderFormWithProps(props)}
      </InputArea>
    </div>
  );
};

Messages.propTypes = {
  source: PropTypes.object,
};

renderFormWithProps.propTypes = {
  inputValue: PropTypes.string.isRequired,
  routerId: PropTypes.string.isRequired,
  updateInput: PropTypes.func.isRequired,
  socket: PropTypes.object.isRequired,
};

export default Messages;
