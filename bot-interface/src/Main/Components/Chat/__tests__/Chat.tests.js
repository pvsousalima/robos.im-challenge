import React from 'react';
import renderer from 'react-test-renderer';
import ChatItem from '../ChatItem/ChatItem';
import ChatMenu from '../ChatMenu/ChatMenu';
import InputArea from '../InputArea/InputArea';
import Messages from '../Messages/Messages';
import MessagesArea from '../MessagesArea/MessagesArea';
import NoMessagesComponent from '../NoMessagesComponent/NoMessagesComponent';
import NoMessagesFoundComponent from '../NoMessagesFoundComponent/NoMessagesFoundComponent';
import ReceivedMessage from '../ReceivedMessage/ReceivedMessage';
import SentMessage from '../SentMessage/SentMessage';

describe('Rendering Layout', () => {
	it('renders ChatItem correctly', () => {
		const tree = renderer.create(
			<ChatItem name={'test'} date={12}/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders ChatMenu correctly', () => {
		const tree = renderer.create(
			<ChatMenu source={{}}/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders InputArea correctly', () => {
		const tree = renderer.create(
			<InputArea children={<div/>}/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders Messages correctly', () => {
		const tree = renderer.create(
			<Messages/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders MessagesArea correctly', () => {
		const tree = renderer.create(
			<MessagesArea/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders NoMessagesComponent correctly', () => {
		const tree = renderer.create(
			<NoMessagesComponent/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders NoMessagesFoundComponent correctly', () => {
		const tree = renderer.create(
			<NoMessagesFoundComponent/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders ReceivedMessage correctly', () => {
		const tree = renderer.create(
			<ReceivedMessage text={'text'} date={123}/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

	it('renders SentMessage correctly', () => {
		const tree = renderer.create(
			<SentMessage text={'text'} date={123}/>
		).toJSON();
		expect(tree).toMatchSnapshot();
	});

})
