import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './styles';

const ChatItem = ({ name, date }) => (
  <div style={styles.chatItemContainer}>
    <div style={styles.nameContainer}>
      {name}
    </div>
    <div style={styles.dateContainer}>
      {`${moment.unix(date).fromNow()}`}
    </div>
  </div>
);

ChatItem.propTypes = {
  name: PropTypes.string.isRequired,
  date: PropTypes.number.isRequired,
};

export default ChatItem;
