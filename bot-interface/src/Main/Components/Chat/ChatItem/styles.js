const styles = {
  chatItemContainer: {
    width: '100%',
    height: '80px',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    display: 'flex',
    textDecoration: 'none',
  },
  nameContainer: {
    fontFamily: 'Roboto',
    fontWeight: '600',
    padding: 6,
  },

  dateContainer: {
    fontFamily: 'Roboto',
    fontWeight: '300',
  },
};

export default styles;
