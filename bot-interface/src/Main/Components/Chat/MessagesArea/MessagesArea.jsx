import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles';

const MessagesArea = ({ children }) => (
  <div style={styles.messageArea}>
    <div style={styles.overflowMessages}>
      {children}
    </div>
  </div>
);

MessagesArea.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object).isRequired,
    PropTypes.object.isRequired,
  ]),
};

export default MessagesArea;
