const styles = {
  messageArea: {
    backgroundColor: 'white',
    justifyContent: 'center',
    maxWidth: '100%',
    margin: 2,
  },

  overflowMessages: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    height: '80vh',
    overflowY: 'scroll',
  },
};

export default styles;
