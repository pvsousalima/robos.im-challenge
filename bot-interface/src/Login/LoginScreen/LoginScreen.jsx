import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import axios from 'axios';
import AccountKit from './AccountKit';
import styles from '../styles';

class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = { logged: false };
    this.onResponse = this.onResponse.bind(this);
  }

  onResponse(response) {
    const { code } = response;
    axios.post('http://localhost:4200/login_success/', {
      code,
    }).then(({ data }) => {
      this.setState({ data, logged: true });
      const path = 'chats';
      browserHistory.push(path);
    });
  }

  render() {
    const { data, loadData } = this.props;

    const {
      appId = '379829512449917',
      csrf = '{{csrf}}',
      version = 'v1.1',
    } = data;

    if (this.state.logged && this.state.data) {
      const path = 'chats';
      browserHistory.push(path);
    }

    return (
      <div style={styles.screenContainer}>
        <div style={styles.loginContainer}>
          <AccountKit
            appId={appId}
            version={version}
            onResponse={this.onResponse}
            loginType="EMAIL"
            csrf={csrf}
            clickedButton={loadData}
          >{p => (<button style={{ margin: 10 }} {...p}>Log in With Email</button>)}
          </AccountKit>

          <AccountKit
            appId={appId}
            version={version}
            onResponse={this.onResponse}
            loginType="PHONE"
            csrf={csrf}
            clickedButton={loadData}
          >{p => (<button style={{ margin: 10 }} {...p}>Log in With Phone</button>)}
          </AccountKit>
        </div>
      </div>
    );
  }
}

LoginScreen.propTypes = {
  loadData: PropTypes.func.isRequired,
  data: PropTypes.shape({}).isRequired,
};


const mapStateToProps = state => ({
  data: state.dataReducer.data,
});

const mapDispatchToProps = dispatch => ({
  loadData: data => dispatch({
    type: 'DATA_FETCH_REQUESTED',
    payload: { data },
  }),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
