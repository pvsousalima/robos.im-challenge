import React from 'react';
import PropTypes from 'prop-types';

class AccountKit extends React.Component {
  constructor(props) {
    super(props);
    this.signIn = this.signIn.bind(this);
    this.state = {
      disabled: false,
    };
  }

  componentDidMount() {
    if (!global.window.AccountKit) {
      ((cb) => {
        const tag = global.document.createElement('script');
        tag.setAttribute(
          'src',
          'https://sdk.accountkit.com/en_US/sdk.js',
        );
        tag.setAttribute('id', 'account-kit');
        tag.setAttribute('type', 'text/javascript');
        tag.onload = cb;
        global.document.head.appendChild(tag);
      })(() => {
        global.window.AccountKit_OnInteractive = this.onLoad.bind(this);
      });
    }
  }

  onLoad() {
    const { appId, csrf, version } = this.props;
    global.window.AccountKit.init({
      appId,
      state: csrf,
      version,
      fbAppEventsEnabled: false,
    });
    this.setState({
      disabled: false,
    });
  }

  signIn() {
    if (this.state.disabled) {
      return;
    }
    const { onResponse } = this.props;
    global.window.AccountKit.login(this.props.loginType, {}, resp => onResponse(resp));
  }

  render() {
    const disabled = this.state.disabled || this.props.disabled;
    const { clickedButton } = this.props;
    return this.props.children({
      onClick: () => {
        this.signIn();
        clickedButton();
      },
      disabled,
    });
  }
}

AccountKit.propTypes = {
  csrf: PropTypes.string.isRequired,
  appId: PropTypes.string.isRequired,
  version: PropTypes.string.isRequired,
  children: PropTypes.func.isRequired,
  onResponse: PropTypes.func.isRequired,
  loginType: PropTypes.oneOf(['PHONE', 'EMAIL']),
  disabled: PropTypes.bool,
  clickedButton: PropTypes.func.isRequired,
};

AccountKit.defaultProps = {
  disabled: false,
  loginType: 'EMAIL',
};

export default AccountKit;
