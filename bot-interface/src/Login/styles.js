const styles = {
  screenContainer: {
    display: 'flex',
    position: 'absolute',
    backgroundColor: '#1dcaff',
    margin: 0,
    top: 0,
    left: 0,
    width: '100vw',
    height: '100vh',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  loginContainer: {
    width: '300px',
    height: '150px',
    borderRadius: '10px',
    backgroundColor: 'white',
    textAlign: 'center',
    boxShadow: '10px 10px 10px rgba(1,1,1,0.1)',
    fontSize: 18,
    fontFamily: 'Roboto',
    fontWeight: '300',
    padding: 18,
  },
};

export default styles;
