import React from 'react';
import FacebookButton from './Components/FacebookButton';
import styles from './styles';

const Login = () => (
  <div style={styles.screenContainer}>
      <div style={styles.loginContainer}>
          Bot Admin
          <FacebookButton />
      </div>
  </div>
);

export default Login;
