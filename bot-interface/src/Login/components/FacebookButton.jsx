import React from 'react';
import { browserHistory, Link } from 'react-router';
import styles from './styles';

const handleClick = () => {
  const path = 'login';
  browserHistory.push(path);
};

const FacebookButton = () => (
  <Link href="" onClick={handleClick} style={styles.buttonContainer}>
    <img height={40} width={40} alt="fblogin" src={require('../Static/fb_icon.png')} />
    <div style={styles.textContainer}>
		  Login with Facebook
    </div>
  </Link>
);

export default FacebookButton;
