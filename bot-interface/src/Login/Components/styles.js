const styles = {
  buttonContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 20,
    backgroundColor: '#3b5998',
    textDecoration: 'none',
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
    color: 'white',
    textAlign: 'center',
  },
};

export default styles;
