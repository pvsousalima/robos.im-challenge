import React from 'react';
import io from 'socket.io-client';
import { browserHistory, Router, Route } from 'react-router';
import { SocketProvider } from 'socket.io-react';

import Login from '../Login/Login';
import LoginScreen from '../Login/LoginScreen/LoginScreen';
import Main from '../Main/Main';
import Provider, { store } from '../Store/Store';

const socket = io.connect('http://localhost:4200');

const App = () => (
  <Provider store={store}>
    <SocketProvider socket={socket}>
      <div>
        <Router history={browserHistory}>
          <Route path="/" component={Login} />
          <Route path="/login" component={LoginScreen} />
          <Route path="/chats" component={Main} />
          <Route path="/chats/:id" component={Main} />
        </Router>
      </div>
    </SocketProvider>
  </Provider>
);

export default App;
